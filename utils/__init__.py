# coding: utf-8

def get_attrs(cls):
    return [
        attr for attr in cls.__dict__.keys()
        if not attr.startswith('__') and not attr.endswith('__')
        and not callable(getattr(cls, attr)) and attr != '_sa_class_manager'
    ]


def new(cls, values):
    dict_obj = {}
    for attr in get_attrs(cls):
        if attr in values:
            dict_obj[attr] = values[attr]
    return cls(**dict_obj)
