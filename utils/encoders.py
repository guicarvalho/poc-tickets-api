# coding: utf-8

from datetime import datetime

from flask.json import JSONEncoder as BaseEncoder


class JSONEncoder(BaseEncoder):
    def default(self, obj):
        from tickets.models import Ticket, TicketData, SubwayTicket, BusTicket
        from users.models import User
        if isinstance(obj, User):
            subway_tickets = []
            bus_tickets = []

            for ticket in obj.subway_tickets:
                data = {
                    'id': ticket.id,
                    'line': ticket.line,
                    'ticket': {
                        'ticket_id': ticket.ticket.id,
                        'alias': ticket.ticket.alias,
                        'last_usage': ticket.ticket.last_usage.strftime('%d/%m/%Y %H:%M:%S'),
                        'ticket_tax_type': ticket.ticket.ticket_tax_type,
                        'passenger_type': ticket.ticket.passenger_type
                    },
                    'ticket_data': {
                        'id': ticket.ticket_data.id,
                        'unit_tax': str(ticket.ticket_data.unit_tax),
                        'quantity': ticket.ticket_data.quantity,
                        'qr_code': ticket.ticket_data.qr_code,
                        'expiration': ticket.ticket_data.expiration.strftime('%d/%m/%Y %H:%M:%S')
                    }
                }
                subway_tickets.append(data)

            for ticket in obj.bus_tickets:
                data = {
                    'id': ticket.id,
                    'ticket': {
                        'ticket_id': ticket.ticket.id,
                        'alias': ticket.ticket.alias,
                        'last_usage': ticket.ticket.last_usage.strftime('%d/%m/%Y %H:%M:%S'),
                        'ticket_tax_type': ticket.ticket.ticket_tax_type,
                        'passenger_type': ticket.ticket.passenger_type
                    },
                    'ticket_data': {
                        'id': ticket.ticket_data.id,
                        'unit_tax': str(ticket.ticket_data.unit_tax),
                        'quantity': ticket.ticket_data.quantity,
                        'qr_code': ticket.ticket_data.qr_code,
                        'expiration': ticket.ticket_data.expiration.strftime('%d/%m/%Y %H:%M:%S')
                    }
                }
                bus_tickets.append(data)

            return {
                'id': obj.id,
                'first_name': obj.first_name,
                'last_name': obj.last_name,
                'cell_phone': obj.cell_phone,
                'email': obj.email,
                'password': obj.password,
                'tickets': {
                    'bus': bus_tickets,
                    'subway': subway_tickets
                }
            }
        elif isinstance(obj, SubwayTicket):
            return {
                'id': obj.id,
                'line': obj.line,
                'ticket': {
                    'ticket_id': obj.ticket.id,
                    'alias': obj.ticket.alias,
                    'last_usage': obj.ticket.last_usage.strftime('%d/%m/%Y %H:%M:%S'),
                    'ticket_tax_type': obj.ticket.ticket_tax_type,
                    'passenger_type': obj.ticket.passenger_type
                },
                'ticket_data': {
                    'id': obj.ticket_data.id,
                    'unit_tax': str(obj.ticket_data.unit_tax),
                    'quantity': obj.ticket_data.quantity,
                    'qr_code': obj.ticket_data.qr_code,
                    'expiration': obj.ticket_data.expiration.strftime('%d/%m/%Y %H:%M:%S')
                }
            }
        elif isinstance(obj, BusTicket):
            return {
                'id': obj.id,
                'ticket': {
                    'ticket_id': obj.ticket.id,
                    'alias': obj.ticket.alias,
                    'last_usage': obj.ticket.last_usage.strftime('%d/%m/%Y %H:%M:%S'),
                    'ticket_tax_type': obj.ticket.ticket_tax_type,
                    'passenger_type': obj.ticket.passenger_type
                },
                'ticket_data': {
                    'id': obj.ticket_data.id,
                    'unit_tax': str(obj.ticket_data.unit_tax),
                    'quantity': obj.ticket_data.quantity,
                    'qr_code': obj.ticket_data.qr_code,
                    'expiration': obj.ticket_data.expiration.strftime('%d/%m/%Y %H:%M:%S')
                }
            }
        return BaseEncoder.default(self, obj)
