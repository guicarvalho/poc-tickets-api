# PoC Tickets API

API (beta) para PoC do aplicativo de Tickets.

## Tecnologias utilizadas
- Python 2.7
- Flask 0.10
- Flask-SQLAlchemy 2.0

## Instalação
É aconselhável que o projeto seja executado em ambiente Unix-like.

Instale o gerenciador de pacotes Python (Pip):
```sh
$ sudo apt-get install python-pip
```

Instale e configure a virtualenv-wrapper. Para mais detalhes de configuração vide [site oficial](http://https://virtualenvwrapper.readthedocs.org/en/latest/):
```sh
$ sudo pip install virtualenvwrapper
$ vi ~/.bash_rc
export WORKON_HOME=~/envs
source /usr/local/bin/virtualenvwrapper.sh
$ [ESC]
$ [: + X + ENTER]
$ source ~/.bash_rc
```

Crie um ambiente virtual para instalar os pacotes necessários para a aplicação:
```sh
$ mkvirtualenv PoCTickets_API
```

Baixe o projeto do repositório e instale as dependencias:
```sh
$ git clone https://bitbucket.org/guicarvalho/poc-tickets-api
$ cd poc-tickets-api
$ pip install -r requirements.txt
```

Rode o projeto:
```sh
$ python app.py
```

O servidor de testes irá escutar no endereço http://127.0.0.1:5000/api/v1.0/. Abaixo os endpoints:


**Para executar os testes foi utilizado curl, você pode instar ou usar algum outro software de sua preferência**
**Para adicionar o Token no header da requisição use -d "token:<token_value>"**
**PS=A validação está desabilitada por enquanto**

## Usuários
### Inserir
```sh
curl -H "Content-type: application/json" -X POST http://127.0.0.1:5000/api/v1.0/users -d '{"first_name": "Guilherme", "last_name":"Carvalho", "cell_phone": "1633327108", "email": "guilherme.carvalho@cast.com.br", "password": "teste"}'
```

### Buscar
```sh
curl -X GET http://127.0.0.1:5000/api/v1.0/users/guilherme.carvalho@cast.com.br
```

### Listar
```sh
curl -X GET http://127.0.0.1:5000/api/v1.0/users
```

### Atualizar
```sh
curl -H "Content-type: application/json" -X PUT http://127.0.0.1:5000/api/v1.0/users/guilherme.carvalho@cast.com.br -d '{"first_name": "Giovanna", "last_name":"Franco", "cell_phone": "16888888888", "email": "guilherme.carvalho@cast.com.br", "password": "teste655"}'
```

### Excluir
```sh
curl -X DELETE http://127.0.0.1:5000/api/v1.0/users/guilherme.carvalho@cast.com.br
```

## Tickets
### Inserir
Para inserir um ticket de ônibus:
```sh
$ curl -H "Content-type: application/json" -X POST http://127.0.0.1:5000/api/v1.0/tickets/bus -d '{"unit_tax": 10.5, "quantity": 1, "qr_code": "yteyewhjb15652532165", "expiration": "26/10/2015 23:59:59", "alias": "ticket onibus", "last_usage": "20/10/2015 10:30:54", "ticket_tax_type": 1, "passenger_type": 1, "user_id": 2}'
```

Para inserir um ticket de metro:
```sh
curl -H "Content-type: application/json" -X POST http://127.0.0.1:5000/api/v1.0/tickets/bus -d '{"unit_tax": 10.5, "quantity": 1, "qr_code": "yteyewhjb15652532165", "expiration": "26/10/2015 23:59:59", "alias": "ticket onibus", "last_usage": "20/10/2015 10:30:54", "ticket_tax_type": 1, "passenger_type": 1, "user_id": 1, "line": "amarela"}'
```

### Atualizar
Para atualizar segue a mesma ideia que o inserir apenas trocando o verbo de POST para PUT.

### Excluir
```sh
curl -X DELETE http://127.0.0.1:5000/api/v1.0/tickets/bus/1
curl -X DELETE http://127.0.0.1:5000/api/v1.0/tickets/subway/1
```

### Buscar
```sh
curl -X GET http://127.0.0.1:5000/api/v1.0/tickets/bus/1
curl -X GET http://127.0.0.1:5000/api/v1.0/tickets/subway/1
```

A API está disponível no [Heroku](https://api-poctickets.herokuapp.com/api/v1.0/users) em uma instancia gratuita, por isso uma lentidão no serviço é esperada pois a instancia é minima.
