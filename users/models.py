# coding: utf-8

from application import db


class User(db.Model):
    __tablename__ = 'user'

    id = db.Column(db.Integer, primary_key=True)
    first_name = db.Column(db.String(50))
    last_name = db.Column(db.String(50))
    cell_phone = db.Column(db.String(12))
    email = db.Column(db.String(120), unique=True)
    password = db.Column(db.String(50))
    subway_tickets = db.relationship('SubwayTicket', backref=('user_subway_tickets'), secondary='join(SubwayTicket, Ticket, SubwayTicket.ticket_id == Ticket.id)')
    bus_tickets = db.relationship('BusTicket', backref=('user_bus_tickets'), secondary='join(BusTicket, Ticket, BusTicket.ticket_id == Ticket.id)')

    def __init__(self, first_name, last_name, cell_phone, email, password, id=None):
        self.bind({
            'id': id,
            'first_name': first_name,
            'last_name': last_name,
            'cell_phone': cell_phone,
            'email': email,
            'password': password
        })

    def __repr__(self):
        return '<User: %s>' % self.id

    def bind(self, obj_dict):
        if obj_dict['id']:
            self.id = obj_dict['id']

        self.first_name = obj_dict['first_name']
        self.last_name = obj_dict['last_name']
        self.cell_phone = obj_dict['cell_phone']
        self.email = obj_dict['email']
        self.password = obj_dict['password']
