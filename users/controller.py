# coding: utf-8

from flask import Blueprint, request, jsonify
from sqlalchemy.exc import IntegrityError

from application import db
from models import User
from utils.http_status import HttpStatus

user_app = Blueprint('user_app', __name__)


@user_app.route('/api/v1.0/users', methods=['GET', 'POST'])
def api_users():
    if request.method == 'POST':
        user = User(**request.get_json(force=True))
        db.session.add(user)
        try:
            db.session.commit()
        except IntegrityError:
            db.session.rollback()
            return jsonify(message='invalid parameters'), HttpStatus.BAD_REQUEST

        return jsonify(user_id=user.id), HttpStatus.CREATED
    else:
        users = User.query.all()

        if not users:
            return jsonify(), HttpStatus.NO_CONTENT

        return jsonify(json_list=users), HttpStatus.OK


@user_app.route('/api/v1.0/users/<user_email>', methods=['GET', 'PUT', 'DELETE'])
def api_users_detail(user_email):
    if request.method == 'GET':
        user = User.query.filter_by(email=user_email).first()
        
        if not user:
            return jsonify(message='resource not found'), HttpStatus.NOT_FOUND

        return jsonify(user=user), HttpStatus.OK

    elif request.method == 'PUT':
        user = User.query.filter_by(email=user_email).first()

        if not user:
            return jsonify(message='user not found'), HttpStatus.NOT_FOUND

        user.bind(request.get_json(force=True))
        db.session.commit()

        return jsonify(message='resource updated successfully'), HttpStatus.OK

    else:
        user = User.query.filter_by(email=user_email).delete()

        if not user:
            return jsonify(message='resource not found'), HttpStatus.NOT_FOUND

        db.session.commit()

        return jsonify(message='resource deleted successfully'), HttpStatus.OK
