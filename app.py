# coding: utf-8

from flask import Flask, request, jsonify

from tickets.controller import ticket_app
from users.controller import user_app
from application import create_app, db

app = create_app()
app.register_blueprint(ticket_app)
app.register_blueprint(user_app)


if __name__ == '__main__':
    db.create_all()
    app.run()
