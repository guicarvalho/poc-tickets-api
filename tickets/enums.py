# coding: utf-8


class TicketTaxType(object):
    UNIQUE, HOURLY, DAILY, WEEKEND, AREA = range(1, 6)


class TicketPassengerType(object):
    CHILD, ADULT, ELDERLY = range(1, 4)
