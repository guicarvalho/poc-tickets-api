# coding: utf-8

from flask import Blueprint, request, jsonify
from sqlalchemy.exc import IntegrityError

from application import db
from auth import login_required
from models import Ticket, TicketData, BusTicket, SubwayTicket
from utils import new
from utils.http_status import HttpStatus

ticket_app = Blueprint('ticket_app', __name__)


@ticket_app.route('/api/v1.0/tickets/bus', methods=['GET', 'POST'])
# @login_required
def api_bus_tickets():
    if request.method == 'POST':
        ticket = new(Ticket, request.get_json(force=True))
        ticket_data = new(TicketData, request.get_json(force=True))
        bus_ticket = new(BusTicket, request.get_json(force=True))

        try:
            ticket.save(ticket)
            ticket_data.save(ticket_data)
        except IntegrityError:
            return jsonify(message='invalid parameters'), HttpStatus.BAD_REQUEST

        bus_ticket.ticket_id = ticket.id
        bus_ticket.ticket_data_id = ticket_data.id

        try:
            bus_ticket.save(bus_ticket)
        except IntegrityError:
            return jsonify(message='invalid parameters'), HttpStatus.BAD_REQUEST

        return jsonify(id=bus_ticket.id), HttpStatus.CREATED
    else:
        tickets = BusTicket.query.all()
        if not tickets:
            return jsonify(), HttpStatus.NO_CONTENT
        return jsonify(json_list=tickets), HttpStatus.OK


@ticket_app.route('/api/v1.0/tickets/bus/<int:ticket_id>', methods=['GET', 'PUT', 'DELETE'])
# @login_required
def api_bus_tickets_detail(ticket_id):
    if request.method == 'GET':
        ticket = BusTicket.query.filter_by(id=ticket_id).first()
        if not ticket:
            return jsonify(message='resource not found'), HttpStatus.NOT_FOUND
        return jsonify(ticket=ticket), HttpStatus.OK

    elif request.method == 'PUT':
        ticket = BusTicket.query.filter_by(id=ticket_id).first()
        if not ticket:
            return jsonify(message='resource not found'), HttpStatus.NOT_FOUND
        ticket.bind(request.get_json(force=True))
        db.session.commit()
        return jsonify(id=ticket.id, message='resource updated successfully'), HttpStatus.OK
    else:
        ticket = BusTicket.query.filter_by(id=ticket_id).delete()
        if not ticket:
            return jsonify(message='resource not found'), HttpStatus.NOT_FOUND
        db.session.commit()
        return jsonify(message='resource deleted successfully'), HttpStatus.OK


@ticket_app.route('/api/v1.0/tickets/subway', methods=['GET', 'POST'])
# @login_required
def api_subway_tickets():
    if request.method == 'POST':
        ticket = new(Ticket, request.get_json(force=True))
        ticket_data = new(TicketData, request.get_json(force=True))
        subway_ticket = new(SubwayTicket, request.get_json(force=True))

        try:
            ticket.save(ticket)
            ticket_data.save(ticket_data)
        except IntegrityError:
            return jsonify(message='invalid parameters'), HttpStatus.BAD_REQUEST

        subway_ticket.ticket_id = ticket.id
        subway_ticket.ticket_data_id = ticket_data.id

        try:
            subway_ticket.save(subway_ticket)
        except IntegrityError:
            return jsonify(message='invalid parameters'), HttpStatus.BAD_REQUEST

        return jsonify(id=subway_ticket.id), HttpStatus.CREATED
    else:
        tickets = SubwayTicket.query.all()
        if not tickets:
            return jsonify(), HttpStatus.NO_CONTENT
        return jsonify(json_list=tickets), HttpStatus.OK


@ticket_app.route('/api/v1.0/tickets/subway/<int:ticket_id>', methods=['GET', 'PUT', 'DELETE'])
# @login_required
def api_subway_tickets_detail(ticket_id):
    if request.method == 'GET':
        ticket = SubwayTicket.query.filter_by(id=ticket_id).first()
        if not ticket:
            return jsonify(message='resource not found'), HttpStatus.NOT_FOUND
        return jsonify(ticket=ticket), HttpStatus.OK

    elif request.method == 'PUT':
        subway_ticket = SubwayTicket.query.filter_by(id=ticket_id).first()
        if not subway_ticket:
            return jsonify(message='resource not found'), HttpStatus.NOT_FOUND
        subway_ticket.bind(request.get_json(force=True))
        db.session.commit()
        return jsonify(id=subway_ticket.id, message='resource updated successfully'), HttpStatus.OK
    else:
        subway_ticket = SubwayTicket.query.filter_by(id=ticket_id).delete()
        if not subway_ticket:
            return jsonify(message='resource not found'), HttpStatus.NOT_FOUND
        db.session.commit()
        return jsonify(message='resource deleted successfully'), HttpStatus.OK
