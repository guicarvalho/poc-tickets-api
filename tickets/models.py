# coding: utf-8

from datetime import datetime

from sqlalchemy.exc import IntegrityError

from application import db
from users.models import User
from utils import get_attrs


class DBOperations(object):
    def save(self, obj):
        try:
            db.session.add(obj)
            db.session.commit()
        except IntegrityError as e:
            db.session.rollback()
            raise e


class Ticket(DBOperations, db.Model):
    __tablename__ = 'ticket'

    id = db.Column(db.Integer, primary_key=True)
    alias = db.Column(db.String(70))
    last_usage = db.Column(db.DateTime)
    ticket_tax_type = db.Column(db.Integer)
    passenger_type = db.Column(db.Integer)

    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))

    def __init__(self, alias, last_usage, ticket_tax_type, passenger_type, user_id=None, id=None):
        self.bind({
            'id': id,
            'user_id': user_id,
            'alias': alias,
            'last_usage': last_usage,
            'ticket_tax_type': ticket_tax_type,
            'passenger_type': passenger_type,
        })

    def __repr__(self):
        return '<Ticket: %s>' % self.id

    def bind(self, obj_dict):
        if obj_dict['id']:
            self.id = obj_dict['id']

        if obj_dict['user_id']:
            self.user_id = obj_dict['user_id']

        self.alias = obj_dict['alias']
        self.ticket_tax_type = obj_dict['ticket_tax_type']
        self.passenger_type = obj_dict['passenger_type']
        self.last_usage = datetime.strptime(obj_dict['last_usage'], '%d/%m/%Y %H:%M:%S')


class TicketData(DBOperations, db.Model):
    __tablename__ = 'ticket_data'

    id = db.Column(db.Integer, primary_key=True)
    unit_tax = db.Column(db.Numeric(10, 2))
    quantity = db.Column(db.Integer)
    qr_code = db.Column(db.String(7200))
    expiration = db.Column(db.DateTime)

    def __init__(self, unit_tax, quantity, qr_code, expiration, id=None):
        self.bind({
            'id': id,
            'unit_tax': unit_tax,
            'quantity': quantity,
            'qr_code': qr_code,
            'expiration': expiration
        })

    def __repr__(self):
        return '<TicketData: %s>' % self.id

    def bind(self, obj_dict):
        if obj_dict['id']:
            self.id = obj_dict['id']

        self.unit_tax = obj_dict['unit_tax']
        self.quantity = obj_dict['quantity']
        self.qr_code = obj_dict['qr_code']
        self.expiration = datetime.strptime(obj_dict['expiration'], '%d/%m/%Y %H:%M:%S')


class BusTicket(DBOperations, db.Model):
    __tablename__ = 'bus_ticket'

    id = db.Column(db.Integer, primary_key=True)
    ticket_id = db.Column(db.Integer, db.ForeignKey('ticket.id'))
    ticket_data_id = db.Column(db.Integer, db.ForeignKey('ticket_data.id'))
    ticket = db.relationship('Ticket', backref=db.backref('bus_ticket', lazy='dynamic'))
    ticket_data = db.relationship('TicketData', backref=db.backref('bus_ticket_data', lazy='dynamic'))

    def __init__(self, ticket_id=None, ticket_data_id=None, id=None):
        self.bind = ({'id': id, 'ticket_id': ticket_id, 'ticket_data_id': ticket_data_id})

    def __repr__(self):
        return '<BusTicket: %s>' % self.id

    def bind(self, obj_dict):
        for attr in obj_dict.keys():
            if attr in get_attrs(SubwayTicket):
                setattr(self, attr, obj_dict.get(attr))
            elif attr in get_attrs(Ticket):
                setattr(self.ticket, attr, obj_dict.get(attr))
            elif attr in get_attrs(TicketData):
                setattr(self.ticket_data, attr, obj_dict.get(attr))


class SubwayTicket(DBOperations, db.Model):
    __tablename__ = 'subway_ticket'

    id = db.Column(db.Integer, primary_key=True)
    line = db.Column(db.String(30))
    ticket_id = db.Column(db.Integer, db.ForeignKey('ticket.id'))
    ticket_data_id = db.Column(db.Integer, db.ForeignKey('ticket_data.id'))
    ticket = db.relationship('Ticket', backref=db.backref('subway_ticket', lazy='dynamic'))
    ticket_data = db.relationship('TicketData', backref=db.backref('subway_ticket_data', lazy='dynamic', uselist=True))

    def __init__(self, line, ticket_id=None, ticket_data_id=None, id=None):
        self.bind({'id': id, 'ticket_id': ticket_id, 'ticket_data_id': ticket_data_id, 'line': line})

    def __repr__(self):
        return '<SubwayTicket: %s>' % self.id

    def bind(self, obj_dict):
        for attr in obj_dict.keys():
            if attr in get_attrs(SubwayTicket):
                setattr(self, attr, obj_dict.get(attr))
            elif attr in get_attrs(Ticket):
                setattr(self.ticket, attr, obj_dict.get(attr))
            elif attr in get_attrs(TicketData):
                setattr(self.ticket_data, attr, obj_dict.get(attr))
