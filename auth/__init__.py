# coding: utf-8

import requests

from functools import wraps

from flask import request, jsonify

from utils.http_status import HttpStatus


def login_required(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        token = request.headers.get('token-google-api')
        response = requests.get('https://www.googleapis.com/oauth2/v1/tokeninfo?access_token=%s' % token)
        if 'error' in response.json():
            return jsonify(message='invalid token'), HttpStatus.UNAUTHORIZED
        return f(*args, **kwargs)
    return decorated_function
