# coding: utf-8

import os

from flask import Flask
from flask.ext.sqlalchemy import SQLAlchemy

from utils.encoders import JSONEncoder


def create_app():
    app = Flask(__name__)
    app.json_encoder = JSONEncoder

    APP_DIR = os.path.dirname(os.path.dirname(__file__))
    app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///%s/PoCTickets.db' % APP_DIR  # local
    # app.config['SQLALCHEMY_ECHO'] = True
    # app.config['SQLALCHEMY_DATABASE_URI'] = os.environ['DATABASE_URL']  # server

    return app


def get_db():
    app = create_app()
    db = SQLAlchemy(app)
    return db

app = create_app()
db = get_db()
